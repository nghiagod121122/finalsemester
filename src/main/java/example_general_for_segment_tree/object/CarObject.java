package example_general_for_segment_tree.object;

public class CarObject<K,E> extends ObjectGen<K,E> {
    public CarObject(K key, E name) {
        super(key, name);
    }
}
