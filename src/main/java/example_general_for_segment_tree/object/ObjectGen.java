package example_general_for_segment_tree.object;

public class ObjectGen<K,E> implements ObjectsGen<K,E> {
    K key;
    E name;

    public ObjectGen(K key, E name){
        this.key = key;
        this.name = name;
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public E getName() {
        return name;
    }
}
