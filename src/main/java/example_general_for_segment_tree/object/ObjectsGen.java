package example_general_for_segment_tree.object;

public interface ObjectsGen<K,E> {
    K getKey();
    E getName();
}
