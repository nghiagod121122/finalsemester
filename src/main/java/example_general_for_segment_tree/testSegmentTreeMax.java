package example_general_for_segment_tree;

import example_general_for_segment_tree.object.CarObject;
import example_general_for_segment_tree.object.IntegerObject;

public class testSegmentTreeMax {
    public static void main(String[] args) {

        IntegerObject object = new IntegerObject(1);
        IntegerObject object1 = new IntegerObject(2);
        IntegerObject object2 = new IntegerObject(3);
        IntegerObject object3 = new IntegerObject(4);
        IntegerObject[] listObject = new IntegerObject[]{object,object1,object2,object3};
        SegTreeGenMax segtre = new SegTreeGenMax(4);
        segtre.insert(listObject,0,3);
        for(int i =0; i < segtre.size(); i++){
            System.out.println("Integer in segtree is: "+segtre.max(i,i));
        }
        System.out.println("Number has max in segtree is: "+segtre.max(1,4));
        segtre.updateValue(new IntegerObject(10000),1,1);
        System.out.println("Number has max in segtree is: "+segtre.max(1,4));

        System.out.println("=====================================");

        CarObject<Integer,String> car1 = new CarObject(1000,"mercedes");
        CarObject<Integer,String> car2 = new CarObject(3000,"labogini");
        CarObject<Integer,String> car3 = new CarObject(500,"honda");
        CarObject<Integer,String> car4 = new CarObject(2000,"ferrary");
        CarObject<Integer,String>[] listCar = new CarObject[]{car1,car2,car3,car4};
        SegTreeGenMax<Integer,String> segTree = new SegTreeGenMax(4);
        segTree.insert(listCar,0,3);
        for(int i =0; i < segTree.size(); i++){
            System.out.println("Car in listCar is: "+segTree.max(i,i));
        }
        System.out.println("Car has max price in listCar is: "+segTree.max(1,4));
        segTree.updateValue(new CarObject(10000,"mercedes"),1,1);
        System.out.println("Car has max price in listCar is: "+segTree.max(1,4));
    }
}
