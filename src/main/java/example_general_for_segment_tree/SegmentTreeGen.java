package example_general_for_segment_tree;

import example_general_for_segment_tree.object.ObjectGen;

public interface SegmentTreeGen<K,E>{
    void insert(ObjectGen<K,E>[] values, int left, int right);
    void updateValue(ObjectGen<K,E> value, int from, int to);
    int size();
    int height();
    E max(int nearLeft, int nearRight);
}
