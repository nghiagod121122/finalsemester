package example_general_for_segment_tree;

import example_general_for_segment_tree.object.ObjectGen;

public class SegTreeGenMax<K extends Comparable<K>,E> implements SegmentTreeGen {
    ObjectGen<K,E>[] segmentTree;
    private int sz = 0;
    private int left;
    private int right;
    private static final int MIN_VALUE = - Integer.MAX_VALUE;

    public SegTreeGenMax(int sz){ segmentTree = new ObjectGen[sz*4]; }

    public ObjectGen<K,E> maxObjectGen(ObjectGen<K,E> firstObject, ObjectGen<K,E> secondObject){
        if(firstObject.getKey().compareTo(secondObject.getKey()) > 0) return firstObject;
        return secondObject;
    }

    @Override
    public void insert(ObjectGen[] values, int left, int right) {
        this.left = left;
        this.right = right;
        insert(values,0,left,right);
    }

    private void insert(ObjectGen[] values, int node, int left, int right){
        if(left == right) {
            segmentTree[node] = values[left];
            sz++;
            return;
        }
        int mid =(left+right)/2;
        insert(values,node*2+1, left, mid);
        insert(values,node*2+2, mid+1, right);
        segmentTree[node] = maxObjectGen(segmentTree[node*2+1],segmentTree[node*2+2]);
    }


    @Override
    public void updateValue(ObjectGen value, int from, int to) {
        if(from < left || to > right) System.out.println("update failed because range wrong");
        else updateValue(value,0, from, to,left,right);
    }

    private void updateValue(ObjectGen value, int node, int from, int to, int left, int right){
        if(to < left || from > right) return;
        if(left == right){
            segmentTree[node] = value;
            return;
        }
        int mid = (left+right)/2;
        updateValue(value,node*2+1, from, to,left,mid);
        updateValue(value,node*2+2, from, to,mid+1,right);
        segmentTree[node] = maxObjectGen(segmentTree[node*2+1],segmentTree[node*2+2]);
    }

    @Override
    public int size() { return sz; }

    @Override
    public int height() {
        int floor = 0;
        while(Math.pow(2,floor) <= size()) floor++;
        return floor;
    }

    @Override
    public E max(int nearLeft, int nearRight) { return (E) max(0, nearLeft, nearRight, left, right).getName(); }

    private ObjectGen max(int node, int nearLeft, int nearRight, int left, int right ){
        if(nearRight < left || nearLeft > right) return new ObjectGen(MIN_VALUE,MIN_VALUE);
        if(nearLeft <= left && nearRight >= right) return segmentTree[node];
        int mid = (left+right)/2;
        return maxObjectGen(max(node*2+1,nearLeft,nearRight,left,mid),
                max(node*2+2,nearLeft,nearRight,mid+1,right));
    }

}
