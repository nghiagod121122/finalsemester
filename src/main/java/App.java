import controller.SegmentTreeController;
import view.SegmentTreeView;

public class App {
    public static void main(String[] args) {
        SegmentTreeView view = new SegmentTreeView();
        SegmentTreeController segTree = new SegmentTreeController(view);
        segTree.showSegmentTree();
    }
}
