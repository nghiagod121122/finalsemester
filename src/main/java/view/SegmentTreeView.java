package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.Box;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SegmentTreeView extends JFrame implements ActionListener {

    public JPanel panelTree = new JPanel();
    public GroupLayout panelLayout;
    public GroupLayout layout;

    private JButton insertBtn;
    private JButton updateBtn;
    public JButton featuredBtn;
    public JButton resetBtn;

    private JMenuItem maxItemMenu;
    private JMenuItem sumItemMenu;
    public JMenu menu;
    public JMenuBar menuBar;

    public JLabel featuredLabel;
    public JTextField listValues;
    public JTextField value;
    public JTextField rangeUpdate;
    public JTextField rangeFeatured;

    public SegmentTreeView() {
        initComponents();
    }

    private void initComponents() {
        Font font = new Font("Calibri", Font.PLAIN, 16);

        insertBtn = new JButton();
        updateBtn = new JButton();
        featuredBtn = new JButton();
        resetBtn = new JButton();

        listValues = new JTextField();
        value = new JTextField();
        rangeUpdate = new JTextField();
        rangeFeatured = new JTextField();
        featuredLabel = new JLabel();

        menuBar = new JMenuBar();
        menu = new JMenu("Segment tree choices");
        menu.setFont(font);

        maxItemMenu = new JMenuItem("Max segment tree");
        maxItemMenu.setFont(font);
        maxItemMenu.addActionListener(this);

        sumItemMenu = new JMenuItem("Sum segment tree");
        sumItemMenu.setFont(font);
        sumItemMenu.addActionListener(this);

        menu.add(maxItemMenu);
        menu.add(sumItemMenu);
        menuBar.add(menu);
        menuBar.add(Box.createRigidArea(new Dimension(100,40)));
        setJMenuBar(menuBar);

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        insertBtn.setText("Insert");
        insertBtn.setFont(font);
        insertBtn.setBackground(Color.GREEN);
        insertBtn.addActionListener(this);

        updateBtn.setText("Update");
        updateBtn.setFont(font);
        updateBtn.setBackground(Color.GREEN);
        updateBtn.addActionListener(this);

        featuredBtn.setText("Max");
        featuredBtn.setFont(font);
        featuredBtn.setBackground(Color.GREEN);
        featuredBtn.addActionListener(this);

        resetBtn.setText("Reset");
        resetBtn.setFont(font);
        resetBtn.setBackground(Color.GREEN);
        resetBtn.addActionListener(this);

        listValues.setText("Enter values");
        listValues.setFont(font);
        value.setText("Enter value");
        value.setFont(font);
        rangeUpdate.setText("Enter range update");
        rangeUpdate.setFont(font);

        rangeFeatured.setText("Range check max");
        rangeFeatured.setFont(font);
        featuredLabel.setText("Max is: ");
        featuredLabel.setFont(font);

        panelTree.setBorder(new LineBorder(Color.BLACK));
        panelTree.setBackground(Color.ORANGE);
        panelLayout = new GroupLayout(panelTree);
        panelTree.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
                panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
        );
        panelLayout.setVerticalGroup(
             panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGap(680, 680, Short.MAX_VALUE)
        );
        layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(100)
                .addComponent(listValues, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(insertBtn, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(value, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(rangeUpdate, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(rangeFeatured, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(featuredBtn, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(featuredLabel,GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(resetBtn,GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addContainerGap(360, 360))
            .addComponent(panelTree, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(panelTree, 680,680,680)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(listValues, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(insertBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(value, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(rangeUpdate, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(updateBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(rangeFeatured, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(featuredBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(featuredLabel, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                            .addComponent(resetBtn, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)))
                .addGap(10, 10, Short.MAX_VALUE))
            ));
        pack();
    }

    public void insertBtn(ActionListener listener){
        insertBtn.addActionListener(listener);
    }

    public void updateBtn(ActionListener listener) {
        updateBtn.addActionListener(listener);
    }

    public void featuredBtn(ActionListener listener) {
        featuredBtn.addActionListener(listener);
    }

    public void resetBtn(ActionListener listener) { resetBtn.addActionListener(listener);}

    public String listValuesTextField(){
        if(listValues.getText().isEmpty() || listValues.getText().equals("Enter values"))
            JOptionPane.showMessageDialog(this,"insert field is empty!","warning",JOptionPane.WARNING_MESSAGE);
        return listValues.getText();
    }

    public String valueTextField(){
        if(value.getText().isEmpty() || value.getText().equals("Enter value"))
            JOptionPane.showMessageDialog(this,"update field is empty!","warning",JOptionPane.WARNING_MESSAGE);
        return value.getText();
    }

    public String rangeUpdateTextField(){
        if(rangeUpdate.getText().isEmpty() || rangeUpdate.getText().equals("Enter range update"))
            JOptionPane.showMessageDialog(this,"range field is empty!","warning",JOptionPane.WARNING_MESSAGE);
        return rangeUpdate.getText();
    }

    public String rangeFeaturedTextField(){
        if(rangeFeatured.getText().isEmpty() || rangeFeatured.getText().equals("Range check max"))
            JOptionPane.showMessageDialog(this,"range max field is empty!","warning",JOptionPane.WARNING_MESSAGE);
        if(rangeFeatured.getText().isEmpty() || rangeFeatured.getText().equals("Range check sum"))
            JOptionPane.showMessageDialog(this,"range sum field is empty!","warning",JOptionPane.WARNING_MESSAGE);
        return rangeFeatured.getText();
    }

    public void maxItemMenu(ActionListener listener){
        maxItemMenu.addActionListener(listener);
    }

    public void sumItemMenu(ActionListener listener) { sumItemMenu.addActionListener(listener); }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
