package data_struct;

public class SegTree implements SegmentTree {
    int[] segmentTree;
    private int sz = 0;
    private int left;
    private int right;
    private static final int MIN_VALUE = - Integer.MAX_VALUE;

    public SegTree(int sz){ segmentTree = new int[sz*4]; }

    @Override
    public void insertMax(int[] values, int left, int right) {
        this.left = left;
        this.right = right;
        insertMax(values,0,left,right);
    }

    private void insertMax(int[] values, int node, int left, int right){
        if(left == right) {
            segmentTree[node] = values[left];
            sz++;
            return;
        }
        int mid =(left+right)/2;
        insertMax(values,node*2+1, left, mid);
        insertMax(values,node*2+2, mid+1, right);
        segmentTree[node] = Math.max(segmentTree[node*2+1],segmentTree[node*2+2]);
    }

    @Override
    public void insertSum(int[] values, int left, int right) {
        this.left = left;
        this.right = right;
        insertSum(values,0,left,right);
    }

    private void insertSum(int[] values, int node, int left, int right){
        if(left == right) {
            segmentTree[node] = values[left];
            sz++;
            return;
        }
        int mid = (left + right) / 2;
        insertSum(values, node * 2 + 1, left, mid);
        insertSum(values, node * 2 + 2, mid + 1, right);
        segmentTree[node] = segmentTree[node * 2 + 1] + segmentTree[node * 2 + 2];
    }

    @Override
    public void updateValueInMax(int value, int from, int to) {
        if(from < left || to > right) System.out.println("update failed because range wrong");
        else updateValueInMax(value,0, from-1, to-1,left,right);
    }

    private void updateValueInMax(int value, int node, int from, int to, int left, int right){
        if(to < left || from > right) return;
        if(left == right){
            segmentTree[node] = value;
            return;
        }
        int mid = (left+right)/2;
        updateValueInMax(value,node*2+1, from, to,left,mid);
        updateValueInMax(value,node*2+2, from, to,mid+1,right);
        segmentTree[node] = Math.max(segmentTree[node*2+1],segmentTree[node*2+2]);
    }

    @Override
    public void updateValueInSum(int value, int from, int to) {
        if(from < left || to > right) System.out.println("update failed because range wrong");
        updateValueInSum(value,0, from-1, to-1,left,right);
    }

    private void updateValueInSum(int value, int node, int from, int to, int left, int right){
        if(to < left || from > right) return;
        if(left == right){
            segmentTree[node] = value;
            return;
        }
        int mid = (left+right)/2;
        updateValueInSum(value,node*2+1, from, to,left,mid);
        updateValueInSum(value,node*2+2, from, to,mid+1,right);
        segmentTree[node] = segmentTree[node * 2 + 1] + segmentTree[node * 2 + 2];
    }

    @Override
    public int size() { return sz; }

    @Override
    public int height() {
        int floor = 0;
        while(Math.pow(2,floor) <= size()) floor++;
        return floor;
    }

    @Override
    public int max(int nearLeft, int nearRight) {
        return max(0, nearLeft-1, nearRight-1, left, right);
    }

    private int max(int node, int nearLeft, int nearRight, int left, int right ){
        if(nearRight < left || nearLeft > right) return MIN_VALUE;
        if(nearLeft <= left && nearRight >= right) return segmentTree[node];
        int mid = (left+right)/2;
        return Math.max(max(node*2+1,nearLeft,nearRight,left,mid),
                max(node*2+2,nearLeft,nearRight,mid+1,right));
    }

    @Override
    public int sum(int nearLeft, int nearRight) {
        return sum(0, nearLeft-1, nearRight-1, left, right);
    }

    private int sum(int node, int nearLeft, int nearRight, int left, int right ){
        if(nearRight < left || nearLeft > right) return 0;
        if(nearLeft <= left && nearRight >= right) return segmentTree[node];
        int mid = (left+right)/2;
        return sum(node*2+1,nearLeft,nearRight,left,mid) +
                sum(node*2+2,nearLeft,nearRight,mid+1,right);
    }

}
