package data_struct;

public interface SegmentTree {
    void insertMax(int[] values, int left, int right);
    void insertSum(int[] values, int left, int right);
    void updateValueInMax(int value, int from, int to);
    void updateValueInSum(int value, int from, int to);
    int size();
    int height();
    int max(int nearLeft, int nearRight);
    int sum(int nearLeft, int nearRight);
}
