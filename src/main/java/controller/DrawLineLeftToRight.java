package controller;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

public class DrawLineLeftToRight extends JComponent implements ActionListener {
    int x1;int y1;int x2;int y2;int limited;
    private final Color color;
    private final float sizeLine;

    Timer time = new Timer(1,this);
    public DrawLineLeftToRight(int x1, int y1, int x2, int y2, int limited, Color color, float sizeLine) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.limited = limited;
        this.color = color;
        this.sizeLine = sizeLine;
        time.start();
    }

    public void animateLine(Graphics2D g) {
        g.setStroke(new BasicStroke(sizeLine));
        g.drawLine(x1, y1, x2, y2);
    }

    public void actionPerformed(ActionEvent arg0) {
        if(limited <= 100){
            if (x2 - x1 <= limited) {
                x2++;y2++;
                repaint();
            }
        }else{
            if (x2 - x1 <= limited) {
                x2+=2;y2++;
                repaint();
            }
        }
    }

    public void paintComponent(Graphics newG){
        super.paintComponent(newG);
        Graphics2D g2d = (Graphics2D)newG;
        g2d.setColor(color);
        animateLine(g2d);
    }
}
