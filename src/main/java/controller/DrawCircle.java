package controller;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JComponent;
import javax.swing.Timer;

public class DrawCircle extends JComponent implements ActionListener {
    private final int x;
    private final int y;
    private final Color color;

    Timer tm = new Timer(100, this);
    boolean grow = true;
    int XDiameter = 0;
    int YDiameter = 0;
    int diameter;

    public DrawCircle(int x, int y,Color color,int diameter) {
        this.x = x;
        this.y = y;
        this.diameter = diameter;
        this.color = color;
        tm.start();
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.setColor(color);
        g.fillOval(x, y, XDiameter, YDiameter);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        SuperSizeCircle();
        repaint();
    }

    public void SuperSizeCircle() {
        grow = XDiameter < diameter;
        if (grow) {
            XDiameter += 3;
            YDiameter += 3;

        }
    }
}
