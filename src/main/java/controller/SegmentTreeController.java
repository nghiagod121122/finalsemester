package controller;

import data_struct.SegTree;
import data_struct.SegmentTree;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JLabel;
import view.SegmentTreeView;

import javax.swing.GroupLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SegmentTreeController {
    private final Font font = new Font("Calibri", Font.PLAIN, 16);

    private final SegmentTreeView view;
    private SegmentTree segTree;
    private int[] sub;
    private static final String NOT_DRAW = "Not Draw" ;
    private static final String DRAW = "Draw" ;

    private int left;
    private int right;
    private final int HORIZONTAL_SIZE = 1600;
    private final int VERTICAL_SIZE = 680;
    private int heightTree = heightTree();
    private final int preferSz = GroupLayout.PREFERRED_SIZE;

    DrawLineRightToLeft lineLeft;
    DrawLineLeftToRight drawLineLeftToRight;
    DrawCircle circle;
    JLabel label;

    public GroupLayout.ParallelGroup horizontal;
    public GroupLayout.ParallelGroup vertical;
    private final GroupLayout PANEL_LAYOUT;

    public SegmentTreeController(SegmentTreeView view){
        this.view = view;
        view.insertBtn(new InsertBtn());
        view.updateBtn(new UpdateBtn());
        view.featuredBtn(new featureBtn());
        view.maxItemMenu(new MaxItemMenu());
        view.sumItemMenu(new SumItemMenu());
        view.resetBtn(new ResetBtn());
        horizontal = view.panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING);
        vertical = view.panelLayout.createParallelGroup(GroupLayout.Alignment.LEADING);
        PANEL_LAYOUT = view.panelLayout;
    }

    public void showSegmentTree(){
        view.setVisible(true);
    }

    public boolean checkJMenuItem(){
        return view.menu.getText().equals("Max segment tree");
    }

    public void setPanel(){
        PANEL_LAYOUT.setHorizontalGroup(horizontal);
        PANEL_LAYOUT.setVerticalGroup(vertical);
    }

    public int heightTree(){
        return heightTree;
    }

    private void drawLineRightToLeft(int lineX1, int lineY1, int limited, Color color, float sizeLine){
        lineLeft = new DrawLineRightToLeft(lineX1,lineY1,lineX1,lineY1,limited,color,sizeLine);
        horizontal.addComponent(lineLeft, preferSz, HORIZONTAL_SIZE, preferSz);
        vertical.addComponent(lineLeft,preferSz, VERTICAL_SIZE, preferSz);
    }

    private void drawLineLeftToRight(int lineX1, int lineY1, int limited, Color color, float sizeLine){
        drawLineLeftToRight = new DrawLineLeftToRight(lineX1,lineY1,lineX1,lineY1,limited,color,sizeLine);
        horizontal.addComponent(drawLineLeftToRight, preferSz, HORIZONTAL_SIZE, preferSz);
        vertical.addComponent(drawLineLeftToRight,preferSz, VERTICAL_SIZE, preferSz);
    }

    private void drawCircle(int cirX, int cirY, Color colorCircle, Color colorValue, int diameter,
                            int value, int heightTextTimeTwo, int widthTextTimeTwo){

        circle = new DrawCircle(cirX,cirY, colorCircle, diameter);
        label  = new JLabel(String.valueOf(value),JLabel.CENTER);
        label.setFont(font);
        label.setForeground(colorValue);

        horizontal.addComponent(label, widthTextTimeTwo, widthTextTimeTwo, widthTextTimeTwo)
                .addComponent(circle, preferSz, HORIZONTAL_SIZE, preferSz);
        vertical.addComponent(label, heightTextTimeTwo, heightTextTimeTwo, heightTextTimeTwo)
                .addComponent(circle,preferSz, VERTICAL_SIZE, preferSz);
    }

    private void traveseInsert(int[] values, int right){
        this.left = 0;
        this.right = right;
        insertTravese(values,0, 0,right, 750,30,735,0,heightTree()*90);
    }

    private void insertTravese(int[] values, int node, int left, int right,
                               int lineX, int lineY, int cirX, int cirY, int limited){
        if(left == right) {
            sub[node] = values[left];
            drawCircle(cirX,cirY,Color.RED,Color.white, 30, sub[node], cirY*2+30, cirX*2+30);
            return;
        }

        int mid =(left+right)/2;
        drawLineRightToLeft(lineX-5,lineY, limited-20,Color.BLUE,2f);
        drawLineLeftToRight(lineX + 5, lineY, limited-20,Color.BLUE,2f);

        if(limited > 100) {
            insertTravese(values, node * 2 + 1, left, mid,
                    lineX - limited, lineY + (limited / 2),
                    cirX - limited, cirY + ((limited + 10) / 2), limited/2);

            insertTravese(values, node * 2 + 2, mid + 1, right,
                    lineX + limited, lineY + (limited / 2),
                    cirX + limited, cirY + ((limited + 10) / 2), limited/2);
        }else {
            insertTravese(values, node * 2 + 1, left, mid,
                    lineX - limited, lineY + limited,
                    cirX - limited, cirY + limited, limited - 50);

            insertTravese(values, node * 2 + 2, mid + 1, right,
                    lineX + limited, lineY + limited,
                    cirX + limited, cirY + limited, limited - 50);
        }
        if(checkJMenuItem()) sub[node] = Math.max(sub[node*2+1],sub[node*2+2]);
        else sub[node] = sub[node*2+1]+ sub[node*2+2];
        drawCircle(cirX,cirY,Color.RED,Color.white, 30, sub[node], cirY*2+30, cirX*2+30);
    }


    public void updateTraverse(int value, int from, int to) {
        traverseUpdate(value,0, from-1, to-1,left,right,750,30,750,0,heightTree()*90);
    }

    private void traverseUpdate(int value, int node, int from, int to, int left, int right
            ,int lineX, int lineY, int cirX, int cirY, int limited){
        if(to < left || from > right) return;
        if(left == right){
            sub[node] = value;
           drawCircle(cirX,cirY,Color.CYAN,Color.RED, 50, sub[node], cirY*2+50, cirX*2+70);
            return;
        }

        int mid = (left+right)/2;
        drawLineRightToLeft(lineX-5,lineY, limited,Color.BLACK,5f);
        drawLineLeftToRight(lineX + 5, lineY, limited,Color.BLACK,5f);

        if(limited > 100) {
            traverseUpdate(value, node * 2 + 1, from, to, left, mid,
                    lineX - limited, lineY + (limited / 2),
                    cirX - limited, cirY + ((limited + 10) / 2), limited/2);

            traverseUpdate(value, node * 2 + 2, from, to, mid + 1, right,
                    lineX + limited, lineY + (limited / 2),
                    cirX + limited, cirY + ((limited + 10) / 2), limited/2);
        }else{
            traverseUpdate(value, node * 2 + 1, from, to, left, mid,
                    lineX - limited, lineY + limited,
                    cirX - limited, cirY + limited, limited / 2);

            traverseUpdate(value, node * 2 + 2, from, to, mid + 1, right,
                    lineX + limited, lineY + limited,
                    cirX + limited, cirY + limited, limited / 2);
        }
        if(checkJMenuItem()) sub[node] = Math.max(sub[node*2+1],sub[node*2+2]);
        else sub[node] = sub[node*2+1]+ sub[node*2+2];
        drawCircle(cirX,cirY,Color.CYAN,Color.RED, 50, sub[node], cirY*2+50, cirX*2+70);
    }


    public void feature(int nearLeft, int nearRight) {
        feature(0, nearLeft-1, nearRight-1, left, right,
                750, 30, 700, 0, heightTree() * 90, DRAW);
    }

    private int feature(int node, int nearLeft, int nearRight, int left, int right,
                         int lineX, int lineY, int cirX, int cirY, int limited, String action){

        if(nearRight < left || nearLeft > right) return 0;
        if(nearLeft <= left && nearRight >= right){
            if(action.equals(DRAW)) drawCircle(cirX,cirY,Color.GREEN,Color.RED, 50, sub[node],
                    cirY*2+50, cirX*2+20);

            return sub[node];
        }
        int mid = (left+right)/2;

        if(action.equals(DRAW)){
            drawLineRightToLeft(lineX-5,lineY, limited,Color.BLACK,5f);
            drawLineLeftToRight(lineX + 5, lineY, limited,Color.BLACK,5f);
        }

        if(limited > 100) {
            feature(node * 2 + 1, nearLeft, nearRight, left, mid,
                    lineX - limited, lineY + (limited / 2),
                    cirX - limited, cirY + ((limited + 10) / 2), limited/2, action);

            feature(node * 2 + 2, nearLeft, nearRight, mid + 1, right,
                    lineX + limited, lineY + (limited / 2),
                    cirX + limited, cirY + ((limited + 10) / 2), limited/2, action);
        }else{
            feature(node * 2 + 1, nearLeft, nearRight, left, mid,
                    lineX - limited, lineY + limited,
                    cirX - limited, cirY + limited, limited / 2, action);

            feature(node * 2 + 2, nearLeft, nearRight, mid + 1, right,
                    lineX + limited, lineY + limited,
                    cirX + limited, cirY + limited, limited / 2, action);
        }
        int value;
        if(checkJMenuItem())
            value = Math.max(feature(node * 2 + 1, nearLeft, nearRight, left, mid,
                    lineX - limited, lineY + (limited / 2),
                    cirX - limited, cirY + ((limited + 10) / 2), limited / 2, NOT_DRAW),
                    feature(node * 2 + 2, nearLeft, nearRight, mid + 1, right,
                            lineX - limited, lineY + (limited / 2),
                            cirX - limited, cirY + ((limited + 10) / 2), limited / 2, NOT_DRAW));
        else
            value = feature(node * 2 + 1, nearLeft, nearRight, left, mid,
                    lineX - limited, lineY + (limited / 2),
                    cirX - limited, cirY + ((limited + 10) / 2), limited / 2, NOT_DRAW) +
                    feature(node * 2 + 2, nearLeft, nearRight, mid + 1, right,
                            lineX + limited, lineY + (limited / 2),
                            cirX + limited, cirY + ((limited + 10) / 2), limited / 2, NOT_DRAW);

        drawCircle(cirX,cirY,Color.GREEN,Color.RED, 50, value, cirY*2+50, cirX*2+20);
        return value;
    }

    class InsertBtn implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String valueListTextField = view.listValuesTextField();
            String[] splitValue = valueListTextField.split(",");
            int[] valueIn = new int[splitValue.length];

            for(int ele = 0; ele < splitValue.length; ele++) valueIn[ele] = Integer.parseInt(splitValue[ele]);
            segTree = new SegTree(valueIn.length);
            if(checkJMenuItem()) segTree.insertMax(valueIn,0,valueIn.length-1);
            else segTree.insertSum(valueIn,0,valueIn.length-1);

            heightTree = segTree.height();
            sub = new int[valueIn.length*4];
            traveseInsert(valueIn, valueIn.length-1);
            setPanel();
        }
    }

    class UpdateBtn implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String valueTextField = view.valueTextField();
            String rangeTextField = view.rangeUpdateTextField();
            int value = Integer.parseInt(valueTextField);
            String[] splitRange = rangeTextField.split(",");
            int[] rangeUpdate = new int[splitRange.length];

            for(int ele = 0; ele < splitRange.length; ele++) rangeUpdate[ele] = Integer.parseInt(splitRange[ele]);
            if(checkJMenuItem()) segTree.updateValueInMax(value,rangeUpdate[0],rangeUpdate[1]);
            else segTree.updateValueInSum(value,rangeUpdate[0],rangeUpdate[1]);

            updateTraverse(value,rangeUpdate[0],rangeUpdate[1]);
            setPanel();
        }
    }

    class featureBtn implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String rangeMax = view.rangeFeaturedTextField();
            String[] splitRange = rangeMax.split(",");
            int[] rangeUpdate = new int[splitRange.length];

            for(int ele = 0; ele < splitRange.length;ele++) rangeUpdate[ele] = Integer.parseInt(splitRange[ele]);
            if(checkJMenuItem()) view.featuredLabel.setText("Max is: " + segTree.max(rangeUpdate[0], rangeUpdate[1]));
            else view.featuredLabel.setText("Sum is: " + segTree.sum(rangeUpdate[0], rangeUpdate[1]));

            feature(rangeUpdate[0],rangeUpdate[1]);
            setPanel();
        }
    }

    private class ResetBtn implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            view.dispose();
            new SegmentTreeController(new SegmentTreeView()).showSegmentTree();
        }
    }

    class MaxItemMenu implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.menu.setText("Max segment tree");
            view.featuredBtn.setText("Max");
            view.featuredLabel.setText("Max is: ");
            view.rangeFeatured.setText("Range check max");
        }
    }

    class SumItemMenu implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            view.menu.setText("Sum segment tree");
            view.featuredBtn.setText("Sum");
            view.featuredLabel.setText("Sum is: ");
            view.rangeFeatured.setText("Range check sum");
        }
    }
}
